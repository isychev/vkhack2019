var express = require('express');
var app = express();

app.use(express.static('build'));

app.get('/api', function (req, res) {
  res.send('Hello World vk!');
});

app.listen(process.env.PORT || 3000 , function () {
  console.log('Example app listening on port 3000 !');
});